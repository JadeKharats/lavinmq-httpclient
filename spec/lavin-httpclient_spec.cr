require "./spec_helper"

describe Lavin::Httpclient do
  describe Lavin::Httpclient::User do
    describe "#all" do
      it "return guest user from a new instance" do
        users = Lavin::Httpclient::User.all
        users.size.should be >= 1
        users[0].name.should eq "guest"
      end
    end
    describe "#all_with_no_permissions" do
    end
    describe "#bulk_delete_users" do
    end
    describe "#get_by_name" do
      it "return guest user by his name" do
        user = Lavin::Httpclient::User.get_by_name("guest")
        user.name.should eq "guest"
      end
    end
    describe "#create_or_update" do
      it "create an user by name and password" do
        Lavin::Httpclient::User.create_or_update(name: "toto", password: "titi")
        user = Lavin::Httpclient::User.get_by_name("toto")
        user.name.should eq "toto"
      end
      it "update tags by user name" do
        Lavin::Httpclient::User.create_or_update(name: "toto", tags: [Lavin::Httpclient::Tags::Monitoring, Lavin::Httpclient::Tags::Management])
        user = Lavin::Httpclient::User.get_by_name("toto")
        user.tags.should eq "monitoring,management"
      end
    end
    describe "#delete" do
      it "delete an user by name" do
        Lavin::Httpclient::User.delete("toto")
        expect_raises(Lavin::Httpclient::HTTPException) do
          Lavin::Httpclient::User.get_by_name("toto")
        end
      end
    end
    describe "#list_permissions" do
    end
  end

  describe Lavin::Httpclient::Status do
    it "return status with void message and queue after init" do
      status = Lavin::Httpclient::Status.get
      status.lavinmq_version.should be_a String
      status.listeners[0].protocol.should eq "amqp"
      status.node.size.should be >= 10
      status.uptime.should be >= 0
      status.object_totals.connections.should eq 0
      status.object_totals.channels.should eq 0
      status.object_totals.exchanges.should be >= 6
      status.object_totals.queues.should be >= 0
      status.queue_totals.messages.should eq 0
      status.queue_totals.messages_ready.should eq 0
      status.queue_totals.messages_unacknowledged.should eq 0
    end
  end
  describe Lavin::Httpclient::Vhost do
    it "manage Vhost" do
      vhosts = Lavin::Httpclient::Vhost.all
      vhosts[0].name.should eq "/"
      Lavin::Httpclient::Vhost.create_or_update("vhost_create")
      vhosts = Lavin::Httpclient::Vhost.all
      vhosts[-1].name.should eq "vhost_create"
      Lavin::Httpclient::Vhost.purge_and_close_consumers("vhost_create", true, "/backup")
      vhost = Lavin::Httpclient::Vhost.get("vhost_create")
      vhost.name.should eq "vhost_create"
      vhost.messages.should eq 0
      Lavin::Httpclient::Vhost.delete("vhost_create")
      expect_raises(Lavin::Httpclient::HTTPException) do
        Lavin::Httpclient::Vhost.get("vhost_create")
      end
    end
  end
  describe Lavin::Httpclient::Queue do
    vhost_name = "vhost_queues_test"
    Lavin::Httpclient::Vhost.create_or_update(vhost_name)
    it "create, list and call a queue in vhost" do
      queue_name = "test_queue_in_vhost"
      Lavin::Httpclient::Queue.create_or_update(queue_name, vhost_name)
      queues = Lavin::Httpclient::Queue.all
      queues_in_vhost = Lavin::Httpclient::Queue.all(vhost_name)
      queues[0].name.should eq queue_name
      queues_in_vhost[0].name.should eq queue_name
      queue = Lavin::Httpclient::Queue.get(queue_name, vhost_name)
      queue.name.should eq queue_name
      Lavin::Httpclient::Queue.pause(queue_name, vhost_name)
      Lavin::Httpclient::Queue.resume(queue_name, vhost_name)
      Lavin::Httpclient::Queue.purge(queue_name, vhost_name)
      Lavin::Httpclient::Queue.delete(queue_name, vhost_name)
      expect_raises(Lavin::Httpclient::HTTPException) do
        Lavin::Httpclient::Queue.get(queue_name, vhost_name)
      end
    end
  end

  describe Lavin::Httpclient::Permission do
    configure = read = write = ".*"
    vhost = "vhost_permissions"
    user = "all_permission_user"
    Lavin::Httpclient::Vhost.create_or_update(vhost)
    Lavin::Httpclient::User.create_or_update(name: user, password: "titi")
    it "create, list, delete permissions in vhost" do
      Lavin::Httpclient::Permission.create_or_update(user, vhost, configure, read, write)
      Lavin::Httpclient::Permission.all
      permissions_by_vhost = Lavin::Httpclient::Permission.get(user, vhost)
      permissions_by_vhost.user.should eq user
      Lavin::Httpclient::Permission.delete(user, vhost)
    end
  end

  describe Lavin::Httpclient::VhostLimit do
    vhost = "vhost_limit"
    Lavin::Httpclient::Vhost.create_or_update(vhost)
    it "set limit for a vhost" do
      max_connections = 9
      max_queues = 8
      Lavin::Httpclient::VhostLimit.set_max_connections(vhost, max_connections)
      Lavin::Httpclient::VhostLimit.set_max_queues(vhost, max_queues)
      vhost_limits = Lavin::Httpclient::VhostLimit.all
      vhost_limits[0].vhost.should eq vhost
      vhost_limit = Lavin::Httpclient::VhostLimit.get(vhost)
      vhost_limit[0].value.max_connections.should eq max_connections
      vhost_limit[0].value.max_queues.should eq max_queues
    end
  end

  describe Lavin::Httpclient::Exchange do
    vhost = "vhost_exchange"
    Lavin::Httpclient::Vhost.create_or_update(vhost)
    it "create list and delete exchange for a vhost" do
      etype = "direct"
      exchange_name = "exchange_test"
      Lavin::Httpclient::Exchange.create_or_update(exchange_name, vhost, etype)
      exchanges = Lavin::Httpclient::Exchange.all
      exchange_in_all = exchanges.select { |exchange| exchange.name == exchange_name }
      exchange_in_all.size.should eq 1
      exchanges_by_vhost = Lavin::Httpclient::Exchange.all(vhost)
      exchanges_by_vhost[-1].name.should eq exchange_name
      exchange = Lavin::Httpclient::Exchange.get(exchange_name, vhost)
      exchange.name.should eq exchange_name
      Lavin::Httpclient::Exchange.delete(exchange_name, vhost)
      expect_raises(Lavin::Httpclient::HTTPException) do
        Lavin::Httpclient::Exchange.get(exchange_name, vhost)
      end
    end
  end

  describe Lavin::Httpclient::Policy do
    vhost = "vhost_policy"
    Lavin::Httpclient::Vhost.create_or_update(vhost)
    it "create, list, delete policy from vhost" do
      name = "policy_test"
      pattern = "plop"
      definition = {"max-length" => 0}
      Lavin::Httpclient::Policy.create_or_update(name, vhost, pattern, definition)
      policies = Lavin::Httpclient::Policy.all
      policy_in_all = policies.select { |policy| policy.name == name }
      policy_in_all.size.should eq 1
      policies_by_vhost = Lavin::Httpclient::Policy.all(vhost)
      policies_by_vhost[-1].name.should eq name
      policy = Lavin::Httpclient::Policy.get(name, vhost)
      policy.pattern.should eq pattern
      Lavin::Httpclient::Policy.delete(name, vhost)
      expect_raises(Lavin::Httpclient::HTTPException) do
        Lavin::Httpclient::Policy.get(name, vhost)
      end
    end
  end

  describe Lavin::Httpclient::Connection do
    with_channel do
      vhost = "vhost_connection"
      Lavin::Httpclient::Vhost.create_or_update(vhost)
      it "list all connection and close them" do
        Lavin::Httpclient::Connection.all
        Lavin::Httpclient::Connection.all(vhost)
        expect_raises(Lavin::Httpclient::HTTPException) do
          Lavin::Httpclient::Connection.close("plop")
        end
        Lavin::Httpclient::Connection.close_all
      end
    end
  end

  describe Lavin::Httpclient::Definition do
    it "export, upload definition" do
      data_dir = "/tmp/lavinmq"
      Dir.mkdir_p(data_dir)
      file_path = "#{data_dir}/definition.json"
      definition = Lavin::Httpclient::Definition.export
      File.write(file_path, definition)
      Lavin::Httpclient::Definition.import(file_path)
      json = JSON.parse(definition)
      keys = json.as_h.keys
      keys.should contain "users"
    end
  end
end
