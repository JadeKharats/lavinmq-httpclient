require "spec"
require "amqp-client"
require "../src/lavin-httpclient"

def with_channel(**args, &)
  host = ENV.fetch("LAVINMQCTL_HOST", "http://guest:guest@localhost:15672")
  conn = AMQP::Client.new(host).connect
  ch = conn.channel
  yield ch
ensure
  conn.try &.close(no_wait: false)
end
