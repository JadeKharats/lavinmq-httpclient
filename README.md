# lavin-httpclient

Client for LavinMQ HTTP API.
Its primary use is to be integrated into the CLI

## Installation

1. Add the dependency to your `shard.yml`:

   ```yaml
   dependencies:
     lavin-httpclient:
       gitlab: jadekharats/lavin-httpclient
   ```

2. Run `shards install`

## Usage

```crystal
require "lavin-httpclient"
```


## Development

Install :
- [guardian](https://github.com/JadeKharats/guardian#installation)
- Docker et Docker-compose

Launch :
- `docker compose up -d` to have a lavinmq instance ready
- `guardian -c` to execute test when you save `spec` or `src` files



## Contributing

Follow gitlab-flow :

- Create an issue.
- Create merge request from issue.
- Pull the merge request branch.
- Remove WIP markers

## Contributors

- [Jade D. Kharats](https://gitlab.com/JadeKharats) - creator and maintainer
