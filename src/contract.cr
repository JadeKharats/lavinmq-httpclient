require "json"

module Contract
  abstract struct Interface
    include JSON::Serializable
  end
end

require "./contract/policy"
require "./contract/connection"
require "./contract/exchange"
require "./contract/vhost"
require "./contract/vhostlimit"
require "./contract/permission"
require "./contract/queue"
require "./contract/status"
require "./contract/user"
