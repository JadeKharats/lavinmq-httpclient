module Lavin::Httpclient
  class User < Model
    ENDPOINT = "/api/users"

    property name : String
    property password_hash : String
    property hashing_algorithm : String
    property tags : String

    def self.all
      resp = http.get ENDPOINT, HEADERS
      handle_response(resp, 200)
      Array(Contract::User).from_json(resp.body)
    end

    def self.get_by_name(name)
      resp = http.get "#{ENDPOINT}/#{name}", HEADERS
      handle_response(resp, 200)
      Contract::User.from_json(resp.body)
    end

    def self.create_or_update(name : String, password : String)
      resp = http.put "#{ENDPOINT}/#{name}", HEADERS, {password: password}.to_json
      handle_response(resp, 201, 204)
    end

    def self.create_or_update(name : String, tags : Array(Tags))
      resp = http.put "/api/users/#{name}", HEADERS, {tags: tags.map(&.to_s).join(",")}.to_json
      handle_response(resp, 204)
    end

    def self.delete(name : String)
      resp = http.delete "/api/users/#{name}", HEADERS
      handle_response(resp, 204)
    end
  end

  enum Tags
    Administrator
    Policymaker
    Monitoring
    Management
    Impersonator
  end
end
