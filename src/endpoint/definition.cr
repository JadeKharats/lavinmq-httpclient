module Lavin::Httpclient
  class Definition < Model
    ENDPOINT = "/api/definitions"

    def self.export
      resp = http.get ENDPOINT
      handle_response(resp, 200)
      resp.body
    end

    def self.import(file_path : String)
      resp = File.open(file_path) do |io|
        http.post ENDPOINT, HEADERS, io
      end
      handle_response(resp, 200)
    end
  end
end
