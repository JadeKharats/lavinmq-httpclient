module Lavin::Httpclient
  class Vhost < Model
    ENDPOINT = "/api/vhosts"

    def self.all
      resp = http.get "/api/vhosts", HEADERS
      handle_response(resp, 200)
      Array(Contract::Vhost).from_json(resp.body)
    end

    def self.create_or_update(name)
      resp = http.put "/api/vhosts/#{name}", HEADERS
      handle_response(resp, 201, 204)
    end

    def self.purge_and_close_consumers(name : String, backup = false, backup_dir_name = "")
      body = if backup
               dir_name = Time.utc.to_unix.to_s if backup_dir_name.empty?
               {
                 "backup":          true,
                 "backup_dir_name": dir_name,
               }
             end
      body ||= {} of String => String
      resp = http.post "/api/vhosts/#{name}/purge_and_close_consumers", HEADERS, body.to_json
      handle_response(resp, 204)
    end

    def self.get(name : String)
      resp = http.get "/api/vhosts/#{name}", HEADERS
      handle_response(resp, 200)
      Contract::Vhost.from_json(resp.body)
    end

    def self.delete(name : String)
      resp = http.delete "/api/vhosts/#{name}", HEADERS
      handle_response(resp, 204)
    end
  end
end
