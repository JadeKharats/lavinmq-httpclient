module Lavin::Httpclient
  class VhostLimit < Model
    ENDPOINT = "/api/vhost-limits"

    def self.set_max_connections(vhost : String, max_connections : Int32)
      url = "#{ENDPOINT}/#{URI.encode_www_form(vhost)}/max-connections"
      body = {value: max_connections}
      resp = http.put url, HEADERS, body.to_json
      handle_response(resp, 204)
    end

    def self.set_max_queues(vhost : String, max_queues : Int32)
      url = "#{ENDPOINT}/#{URI.encode_www_form(vhost)}/max-queues"
      body = {value: max_queues}
      resp = http.put url, HEADERS, body.to_json
      handle_response(resp, 204)
    end

    def self.all
      resp = http.get ENDPOINT
      handle_response(resp, 200)
      Array(Contract::VhostLimit).from_json(resp.body)
    end

    def self.get(vhost : String)
      url = "#{ENDPOINT}/#{URI.encode_www_form(vhost)}"
      resp = http.get url
      handle_response(resp, 200)
      Array(Contract::VhostLimit).from_json(resp.body)
    end
  end
end
