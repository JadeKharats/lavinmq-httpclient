module Lavin::Httpclient
  class Status < Model
    ENDPOINT = "/api/overview"

    def self.get
      resp = http.get ENDPOINT, HEADERS
      handle_response(resp, 200)
      Contract::Status.from_json(resp.body)
    end
  end
end
