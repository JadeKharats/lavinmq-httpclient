module Lavin::Httpclient
  class Connection < Model
    ENDPOINT = "/api/connections"

    def self.all
      resp = http.get ENDPOINT
      handle_response(resp, 200)
      Array(Contract::Connection).from_json(resp.body)
    end

    def self.all(vhost : String)
      url = "/api/vhosts/#{URI.encode_www_form(vhost)}/connections"
      resp = http.get url
      handle_response(resp, 200)
      Array(Contract::Connection).from_json(resp.body)
    end

    def self.close(name : String, reason : String = "Closed via Lavin::Httpclient")
      url = "#{ENDPOINT}/#{name}"
      headers = HEADERS
      headers.add("X-Reason", reason)
      resp = http.delete url, headers
      handle_response(resp, 204)
    end

    def self.close_all(reason : String = "Closed via Lavin::Httpclient")
      all = self.all
      all.each do |connection|
        self.close(connection.name)
      end
    end
  end
end
