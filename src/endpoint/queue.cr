module Lavin::Httpclient
  class Queue < Model
    ENDPOINT = "/api/queues"

    def self.all
      resp = http.get ENDPOINT, HEADERS
      handle_response(resp, 200)
      Array(Contract::Queue).from_json(resp.body)
    end

    def self.all(vhost : String)
      url = "#{ENDPOINT}/#{URI.encode_www_form(vhost)}"
      resp = http.get url, HEADERS
      handle_response(resp, 200)
      Array(Contract::Queue).from_json(resp.body)
    end

    def self.delete(name : String, vhost : String = "/")
      url = "#{ENDPOINT}/#{URI.encode_www_form(vhost)}/#{name}"
      resp = http.delete url
      handle_response(resp, 204)
    end

    def self.create_or_update(
      name : String,
      vhost : String = "/",
      durable = false,
      auto_delete = false,
      arguments : Hash(String, String | UInt32) = Hash(String, String | UInt32).new
    )
      url = "#{ENDPOINT}/#{URI.encode_www_form(vhost)}/#{name}"
      body = {
        "auto_delete": auto_delete,
        "durable":     durable,
        "arguments":   arguments,
      }
      resp = http.put url, HEADERS, body.to_json
      handle_response(resp, 201, 204)
    end

    def self.get(name : String, vhost : String = "/")
      url = "#{ENDPOINT}/#{URI.encode_www_form(vhost)}/#{name}"
      resp = http.get url
      handle_response(resp, 200)
      Contract::Queue.from_json(resp.body)
    end

    def self.purge(name : String, vhost : String = "/")
      url = "#{ENDPOINT}/#{URI.encode_www_form(vhost)}/#{name}/contents"
      resp = http.delete url
      handle_response(resp, 204)
    end

    def self.pause(name : String, vhost : String = "/")
      url = "#{ENDPOINT}/#{URI.encode_www_form(vhost)}/#{name}/pause"
      resp = http.put url
      handle_response(resp, 200, 204)
    end

    def self.resume(name : String, vhost : String = "/")
      url = "#{ENDPOINT}/#{URI.encode_www_form(vhost)}/#{name}/resume"
      resp = http.put url
      handle_response(resp, 200, 204)
    end
  end
end
