module Lavin::Httpclient
  class Permission < Model
    ENDPOINT = "/api/permissions"

    def self.create_or_update(
      user : String,
      vhost : String,
      configure : String,
      read : String,
      write : String
    )
      url = "#{ENDPOINT}/#{URI.encode_www_form(vhost)}/#{user}"
      body = {
        "configure": configure,
        "read":      read,
        "write":     write,
      }
      resp = http.put url, HEADERS, body.to_json
      handle_response(resp, 201, 204)
    end

    def self.all
      resp = http.get ENDPOINT
      handle_response(resp, 200)
      Array(Contract::Permission).from_json(resp.body)
    end

    def self.get(user : String, vhost : String)
      url = "#{ENDPOINT}/#{URI.encode_www_form(vhost)}/#{user}"
      resp = http.get url
      handle_response(resp, 200)
      Contract::Permission.from_json(resp.body)
    end

    def self.delete(user : String, vhost : String)
      url = "#{ENDPOINT}/#{URI.encode_www_form(vhost)}/#{user}"
      resp = http.delete url
      handle_response(resp, 204)
    end
  end
end
