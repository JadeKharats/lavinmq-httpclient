module Lavin::Httpclient
  class Exchange < Model
    ENDPOINT = "/api/exchanges"

    def self.create_or_update(
      name : String,
      vhost : String,
      etype : String,
      auto_delete : Bool = false,
      durable : Bool = false,
      internal : Bool = false,
      delayed : Bool = false,
      arguments : Hash(String, String | UInt32) = Hash(String, String | UInt32).new
    )
      url = "#{ENDPOINT}/#{URI.encode_www_form(vhost)}/#{name}"
      body = {
        "type":        etype,
        "durable":     durable,
        "auto_delete": auto_delete,
        "internal":    internal,
        "delayed":     delayed,
        "arguments":   arguments,
      }
      resp = http.put url, HEADERS, body.to_json
      handle_response(resp, 201, 204)
    end

    def self.all
      resp = http.get ENDPOINT
      handle_response(resp, 200)
      Array(Contract::Exchange).from_json(resp.body)
    end

    def self.all(vhost : String)
      url = "#{ENDPOINT}/#{URI.encode_www_form(vhost)}"
      resp = http.get url
      handle_response(resp, 200)
      Array(Contract::Exchange).from_json(resp.body)
    end

    def self.get(name : String, vhost : String)
      url = "#{ENDPOINT}/#{URI.encode_www_form(vhost)}/#{name}"
      resp = http.get url
      handle_response(resp, 200)
      Contract::Exchange.from_json(resp.body)
    end

    def self.delete(name : String, vhost : String)
      url = "#{ENDPOINT}/#{URI.encode_www_form(vhost)}/#{name}"
      resp = http.delete url
      handle_response(resp, 204)
    end
  end
end
