module Lavin::Httpclient
  class Policy < Model
    ENDPOINT = "/api/policies"

    def self.create_or_update(
      name : String,
      vhost : String,
      pattern : String,
      definition : Hash(String, String | Int32),
      priority : Int32 = 0,
      applyto : String = "All"
    )
      url = "#{ENDPOINT}/#{URI.encode_www_form(vhost)}/#{name}"
      body = {
        "pattern":    pattern,
        "definition": definition,
        "priority":   priority,
        "apply-to":   applyto,
      }
      resp = http.put url, HEADERS, body.to_json
      handle_response(resp, 201, 204)
    end

    def self.all
      resp = http.get ENDPOINT
      handle_response(resp, 200)
      Array(Contract::Policy).from_json(resp.body)
    end

    def self.all(vhost : String)
      url = "#{ENDPOINT}/#{URI.encode_www_form(vhost)}"
      resp = http.get url
      handle_response(resp, 200)
      Array(Contract::Policy).from_json(resp.body)
    end

    def self.get(name : String, vhost : String)
      url = "#{ENDPOINT}/#{URI.encode_www_form(vhost)}/#{name}"
      resp = http.get url
      handle_response(resp, 200)
      Contract::Policy.from_json(resp.body)
    end

    def self.delete(name : String, vhost : String)
      url = "#{ENDPOINT}/#{URI.encode_www_form(vhost)}/#{name}"
      resp = http.delete url
      handle_response(resp, 204)
    end
  end
end
