require "json"
require "http/client"

module Lavin::Httpclient
  abstract class Model
    HEADERS = HTTP::Headers{"Content-Type" => "application/json"}
    include JSON::Serializable

    private def self.http : HTTP::Client?
      host = ENV.fetch("LAVINMQCTL_HOST", "http://guest:guest@localhost:15672")
      uri = URI.parse(host)
      client = HTTP::Client.new(uri)
      client.basic_auth(uri.user, uri.password) if uri.user
      client
    end

    private def self.handle_response(resp, *ok)
      return if ok.includes? resp.status_code
      raise HTTPException.new(resp.status, resp.status_code, resp.body)
    end

    private def self.url_encoded_vhost(name : String)
      URI.encode_www_form(name)
    end
  end

  class HTTPException < Exception
    @status : HTTP::Status
    @status_code : Int32
    @body : String

    def initialize(@status, @status_code, @body)
      @message = "#{@status_code} - #{@status} : #{@body}"
    end
  end
end
