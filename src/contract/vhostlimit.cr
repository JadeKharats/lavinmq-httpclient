module Contract
  struct VhostLimit < Interface
    property vhost : String
    property value : LimitValue
  end

  struct LimitValue < Interface
    @[JSON::Field(key: "max-connections")]
    property max_connections : Int32?
    @[JSON::Field(key: "max-queues")]
    property max_queues : Int32?
  end
end
