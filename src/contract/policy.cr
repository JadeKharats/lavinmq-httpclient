module Contract
  struct Policy < Interface
    property name : String
    property vhost : String
    property pattern : String
    @[JSON::Field(key: "apply-to")]
    property applyto : String
    property priority : Int32
    property definition : Hash(String, String | Int32) = Hash(String, String | Int32).new
  end
end
