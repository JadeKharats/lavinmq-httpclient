module Contract
  struct Vhost < Interface
    property name : String
    property dir : String
    property messages : UInt32
    property messages_unacknowledged : UInt32
    property messages_ready : UInt32
    property message_stats : MessagesStatsVhost
  end

  struct MessagesStatsVhost < Interface
    property ack : UInt32
    property confirm : UInt32
    property deliver : UInt32
    property get : UInt32
    property get_no_ack : UInt32
    property publish : UInt32
    property redeliver : UInt32
    property return_unroutable : UInt32
  end
end
