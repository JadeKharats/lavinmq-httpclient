module Contract
  struct User < Interface
    property name : String
    property password_hash : String
    property hashing_algorithm : String
    property tags : String
  end
end
