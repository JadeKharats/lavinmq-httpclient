module Contract
  struct Queue < Interface
    property name : String
    property? durable : Bool
    property? exclusive : Bool
    property? auto_delete : Bool
    property arguments : Hash(String, String | UInt32)
    property consumers : UInt32
    property vhost : String
    property messages : UInt32
    property ready : UInt32
    property ready_bytes : UInt32
    property ready_avg_bytes : UInt32
    property unacked : UInt32
    property unacked_bytes : UInt32
    property unacked_avg_bytes : UInt32
    property policy : String?
    property exclusive_consumer_tag : String?
    property state : String
    property effective_policy_definition : EffectivePolicyDefinition
    property message_stats : MessagesStatsQueue
    property? internal : Bool?
    property first_message_timestamp : UInt32?
    property last_message_timestamp : UInt32?
  end

  struct MessagesStatsQueue < Interface
    property ack : UInt32
    property ack_details : Details
    property deliver : UInt32
    property deliver_details : Details
    property get : UInt32
    property get_details : Details
    property publish : UInt32
    property publish_details : Details
    property redeliver : UInt32
    property redeliver_details : Details
    property reject : UInt32
    property reject_details : Details
    property return_unroutable : UInt32
    property return_unroutable_details : Details
  end

  struct EffectivePolicyDefinition < Interface
    property name : String?
    property vhost : String?
    property pattern : String?
    @[JSON::Field(key: "apply-to")]
    property applyto : String?
    property priority : UInt32?
  end
end
