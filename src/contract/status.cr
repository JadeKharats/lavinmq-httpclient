module Contract
  struct Status < Interface
    property lavinmq_version : String
    property exchange_types : Array(Hash(String, String))
    property listeners : Array(Listener)
    property message_stats : MessagesStatsStatus
    property node : String
    property object_totals : ObjectTotals
    property queue_totals : QueueTotals
    property recv_oct_details : Details
    property send_oct_details : Details
    property uptime : UInt32
  end

  struct Listener < Interface
    property ip_address : String
    property protocol : String
    property port : UInt32
  end

  struct MessagesStatsStatus < Interface
    property ack : UInt32
    property ack_details : Details
    property deliver : UInt32
    property deliver_details : Details
    property get : UInt32
    property get_details : Details
    property publish : UInt32
    property publish_details : Details
    property redeliver : UInt32
    property redeliver_details : Details
    property reject : UInt32
    property reject_details : Details
  end

  struct ObjectTotals < Interface
    property channels : UInt32
    property connections : UInt32
    property consumers : UInt32
    property exchanges : UInt32
    property queues : UInt32
  end

  struct QueueTotals < Interface
    property messages : UInt32
    property messages_ready : UInt32
    property messages_unacknowledged : UInt32
    property messages_log : Array(UInt32)
    property messages_ready_log : Array(UInt32)
    property messages_unacknowledged_log : Array(UInt32)
  end
end
