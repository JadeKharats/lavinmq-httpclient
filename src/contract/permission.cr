module Contract
  struct Permission < Interface
    property user : String
    property vhost : String
    property configure : String
    property read : String
    property write : String
  end
end
