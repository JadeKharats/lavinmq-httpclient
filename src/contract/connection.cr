module Contract
  struct Connection < Interface
    property channels : Int64
    property connected_at : Int64
    property type : String
    property channel_max : Int64
    property timeout : Int64
    property client_properties : ClientProperties
    property vhost : String
    property user : String
    property protocol : String
    property auth_mechanism : String
    property host : String
    property port : Int64
    property peer_host : String
    property peer_port : Int64
    property name : String
    property? ssl : Bool
    property tls_version : String?
    property cipher : String?
    property state : String
    property send_oct : Int64
    property send_oct_details : Details
    property recv_oct : Int64
    property recv_oct_details : Details
    property channel_created : Int64?
    property channel_created_details : Details?
    property channel_closed : Int64?
    property channel_closed_details : Details?
  end

  struct ClientProperties < Interface
    property capabilities : Capabilities
    property product : String
    property platform : String
    property version : String
    property information : String?
    property connection_name : String
  end

  struct Capabilities < Interface
    property? publisher_confirms : Bool
    property? consumer_cancel_notify : Bool
    property? exchange_exchange_bindings : Bool
    @[JSON::Field(key: "basic.nack")]
    property? basicnack : Bool
    @[JSON::Field(key: "connection.blocked")]
    property? connection_blocked : Bool
    property? authentication_failure_close : Bool
  end

  struct Details < Interface
    property rate : Float32
    property log : Array(Float32)?
  end
end
