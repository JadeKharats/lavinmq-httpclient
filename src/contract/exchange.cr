module Contract
  struct Exchange < Interface
    property type : String
    property? auto_delete : Bool
    property? durable : Bool
    property? internal : Bool
    property name : String
    property vhost : String
    property message_stats : MessagesStatsExchanges
  end

  struct MessagesStatsExchanges < Interface
    property publish_in : Int32
    property publish_in_details : Details
    property publish_out : Int32
    property publish_out_details : Details
    property unroutable : Int32
    property unroutable_details : Details
  end
end
